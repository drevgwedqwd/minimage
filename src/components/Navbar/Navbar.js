import React, { Component } from 'react';
import { ipcRenderer } from 'electron';
import './Navbar.css';

import plusIcon from '../../assets/icons/plus.svg';
import refreshIcon from '../../assets/icons/refresh.svg';
import settingsIcon from '../../assets/icons/settings.svg';

class Navbar extends Component {
  openSettings = () => {
    this.props.onOpenSettings();
  }

  refreshFiles = () => {
    this.props.onRefreshFiles();
  }

  browseFiles = () => {
    ipcRenderer.send('browse-files');
  }

  render() {
    return (
      <header className="App-header">
        <h1 className="App-title">Minimage</h1>
        <div className="App-controls">
          <a onClick={this.browseFiles}><img src={plusIcon} className="controls-svg" alt="Add" /></a>
          <a onClick={this.refreshFiles}><img src={refreshIcon} className="controls-svg" alt="Repeat" /></a>
          <a onClick={this.openSettings}><img src={settingsIcon} className="controls-svg" alt="Settings" /></a>
        </div>
      </header>
    );
  }
}

export default Navbar;
